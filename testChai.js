const chai = require('chai');
const expect = chai.expect;
const math = require('./math');
describe('test chai',()=>{
    it('should compare thing by expect',()=>{
        expect(1).to.equal(1);
    })
    it('should compare another thing by  expect',()=>{
        expect(5>8).to.be.false;
        expect({name : 'max'}).to.deep.equal({name : 'max'});
        expect({name : 'max'}).to.have.property('name').to.equal('max');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('max').to.be.a('String');
        expect('max'.length).to.equal(3);
        expect('max').to.lengthOf(3);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});

describe('Math module',()=>{
    context('Function add1',()=>{
        it('ส่งค่ากลับมาเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number');
        });
        it('add(1,1) ส่งค่ากลับมาเป็นเลข 2',()=>{
            expect(math.add1(1,1)).to.equal(2);
        });
    });
});